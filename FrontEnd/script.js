// add function for adding item in list
function addTodoItem(id, title) {
  const newItem = `  <li data-id="${id}">
    <div class="form-check">
        <label class="form-check-label">
            <input class="checkbox" type="checkbox">${title}
            <i class="input-helper"></i>
        </label>
    </div>
    <i class="remove mdi mdi-close-circle-outline"></i>
     </li>`

  $('ul.todo-list').append(newItem);
}

function addInServer(title) {
  return $.ajax({
    url: `http://localhost:3000/tasks`,
    method: 'post',
  })
}

function handleAdd() {
  const selector = $('#input-text')
  const value = selector.val();
  console.log(value);
  // addTodoItem(null, value); //for front end
  addInServer(value) //for server
    .done(() => {
      console.log("value added to addInServer");
    })
    .fail(() => {
      alert('Could not add item in server')
    })
  $('#input-text').val('');
  getTodos()
}

//add function for deleting item in list
function handleDelete(event) {
  const listItem = event.target.closest("li");
  const id = $(listItem).data().id;
  console.log('before deleting');
  deleteTodoItem(id) //delete element from server
    .done(() => {
      $(listItem).remove(); //delete element from front end
      console.log("item deleted from html and server");
    })
    .fail(() => {
      alert('couldnt delete item')
    })
  console.log('after deleting');
}

function deleteTodoItem(id) {
  return $.ajax({
    url: `http://localhost:3000/tasks/${id}`,
    method: 'DELETE',
  })
}

function handelKeypress(event) {
  if (event.which === 13) {
    handleAdd();
    console.log('added by pressing enter key');
  };
}

function getTodos() {
  console.log('ajax started');
  $.ajax({
      url: `http://localhost:3000/tasks`,
      method: `get`
    })
    .done(function(data) {
      console.log('Received data from server');
      console.log('data', data);
      data.forEach((item) => {
        addTodoItem(item.id, item.details)
      });
    })
    .fail(function(jqXHR, status, error) {
      console.log('data receiving failed from server');
      console.log('err', error);
    });
  console.log('finish ajax');
}

$(document).ready(function() {
  console.log('DOM ready');
  getTodos();
  $('ul.todo-list').on('click', '.remove', handleDelete);

  $('button.btn-primary').click(handleAdd, function() {
    console.log('added by clicking add button');
  });
  $('input#input-text').keypress(handelKeypress)
});