function addItem(todo) {
  var listContainer = document.querySelector('ul.todo-list');

  const newItem = document.createElement('li');

  newItem.innerHTML = `
    <div class="form-check">
      <label class="form-check-label"> 
        <input class="checkbox" type="checkbox" checked="" />${todo}
          <i class="input-helper"></i>
      </label> 
    </div> 
    <i class="remove mdi mdi-close-circle-outline"></i>
  `;
}