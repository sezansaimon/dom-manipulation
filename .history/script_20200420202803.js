const containerSelector = 'ul.todo-list';


function addItem(todo) {
  const listContainer = document.querySelector(containerSelector);
  const newItem = document.createElement('li');

  newItem.innerHTML = `
    <div class="form-check">
      <label class="form-check-label"> 
        <input class="checkbox" type="checkbox"/>${todo}
          <i class="input-helper"></i>
      </label> 
    </div> 
    <i class="remove mdi mdi-close-circle-outline"></i>
  `;

  listContainer.appendChild(newItem);
}

function handleAdd() {
  const inputField = document.querySelector('.add-items input');
  const val = inputField.value;
  addItem(val);
  inputField.value = '';
}

function deleteItem(e){
  const element = e.target
  console.log(e);
}

document.addEventListener('DOMContentLoaded', function() {
  document.querySelector(containerSelector).addEventListener('click', deleteItem);
});
